package com.example.rahmadchallange4
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.rahmadchallange4.datacu.Notes
import com.example.rahmadchallange4.datacu.NotesDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainViewModel (app : Application) : AndroidViewModel(app) {
    // Implement the ViewModel
    var mNote : MutableLiveData<List<Notes>>

    init {
        mNote = MutableLiveData()
        getNote()
    }

    //LiveData Observer
    fun getNoteObservers():MutableLiveData<List<Notes>>{
        return mNote
    }

    //mengambil data untuk di tampilkan
    fun getNote() {
        GlobalScope.launch {
            val dataDao = NotesDatabase.getInstance(getApplication())!!.notesDao()
            val listNote = dataDao.getAllNotes()
            mNote.postValue(listNote)
        }
    }

    fun insert(entityNote: Notes){
        val dataDao = NotesDatabase.getInstance(getApplication())?.notesDao()
        dataDao!!.insertNotes(entityNote)
        getNote()
    }

    fun delete(entityNote: Notes){
        val dataDao = NotesDatabase.getInstance(getApplication())!!.notesDao()
        dataDao?.deleteNotes(entityNote)
        getNote()
    }

    fun update(entityNote: Notes){
        val dataDao = NotesDatabase.getInstance(getApplication())!!.notesDao()
        dataDao?.updateNotes(entityNote)
        getNote()
    }
}