package com.example.rahmadchallange4

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.rahmadchallange4.databinding.ActivityAdapterNotesBinding
import com.example.rahmadchallange4.datacu.Notes
import com.example.rahmadchallange4.datacu.NotesDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.util.ArrayList

class NotesAdapter(var notes: List<Notes>): RecyclerView.Adapter<NotesAdapter.ViewHolder>() {

    var DBNote : NotesDatabase? = null

    class ViewHolder(var binding : ActivityAdapterNotesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind (item : Notes){
            binding.datanotes = item
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = ActivityAdapterNotesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(notes[position])
        //delete note
        holder.binding.deleteBtn.setOnClickListener {
            DBNote = NotesDatabase.getInstance(it.context)
            GlobalScope.async {
                MainViewModel(Application()).delete(notes[position])
                DBNote?.notesDao()?.deleteNotes(notes[position])
                kotlin.run {
                    Navigation.findNavController(it).navigate(R.id.home2)
                }
            }
        }
        //update
        holder.binding.editBtn.setOnClickListener {
            var update = Bundle()
            update.putSerializable("updaters", notes[position])
            Navigation.findNavController(it).navigate(R.id.action_home2_to_editFragment, update)
        }
        //detail
        holder.binding.btnDetail.setOnClickListener {
            var detail = Bundle()
            detail.putSerializable("datanotes", notes[position])
            Navigation.findNavController(it).navigate(R.id.action_home2_to_detailFragment, detail)
        }
    }

    override fun getItemCount(): Int {
        return notes.size
    }

    fun setNotes(itemNote : ArrayList<Notes>){
        this.notes = itemNote
    }
}