package com.example.rahmadchallange4


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rahmadchallange4.databinding.FragmentHomeBinding
import com.example.rahmadchallange4.datacu.Notes
import com.example.rahmadchallange4.datacu.NotesDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class Home : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var viewModel: MainViewModel
    lateinit var share : SharedPreferences
    lateinit var adapter: NotesAdapter
    var DBNote : NotesDatabase? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        share = requireActivity().getSharedPreferences("ACC", Context.MODE_PRIVATE)!!
        val fullname = share.getString("username","username")
        binding.welcomeBar.text = "Welcome, $fullname!"
        Log.d("Homescreen", "Username : $fullname")

        binding.logoutBtn.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.action_home2_to_login)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        //ViewModel
        adapter = NotesAdapter(ArrayList())
        binding.recycleViewNotes.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.recycleViewNotes.adapter = adapter
        //LiveData
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        viewModel.getNoteObservers().observe(viewLifecycleOwner, Observer {
            adapter.setNotes(it as ArrayList<Notes>)
        })
        //Room
        DBNote = NotesDatabase.getInstance(requireContext())
        //Add Note
        binding.addBtn.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.action_home2_to_addNoteFragment)
        }
    }


    override fun onStart() {
        super.onStart()
        GlobalScope.launch {
            var data = DBNote?.notesDao()?.getAllNotes()
            activity?.runOnUiThread {
                adapter = NotesAdapter(data!!)
                binding.recycleViewNotes.layoutManager =
                    LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                binding.recycleViewNotes.adapter = adapter
            }


        }
    }
    }