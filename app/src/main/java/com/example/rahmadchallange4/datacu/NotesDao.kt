package com.example.rahmadchallange4.datacu

import androidx.room.*


@Dao
interface NotesDao {
    @Insert
    fun insertNotes(notes: Notes)

    @Query ("SELECT * FROM Notes ORDER BY id DESC")
    fun getAllNotes() : List<Notes>

    @Update
    fun updateNotes(notes: Notes)

    @Delete
    fun deleteNotes (notes: Notes)

}